import unittest

from Client.main import compte_des_cartes

class TestClient(unittest.TestCase): 
    def test_compte_carte(self): 
        cartes1 = [{'code': 'KS', 'image': 'https://deckofcardsapi.com/static/img/KS.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/KS.svg', 'png': 'https://deckofcardsapi.com/static/img/KS.png'}, 'value': 'KING', 'suit': 'SPADES'},
        {'code': 'KD', 'image': 'https://deckofcardsapi.com/static/img/KD.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/KD.svg', 'png': 'https://deckofcardsapi.com/static/img/KD.png'}, 'value': 'KING', 'suit': 'DIAMONDS'},
        {'code': '6C', 'image': 'https://deckofcardsapi.com/static/img/6C.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/6C.svg', 'png': 'https://deckofcardsapi.com/static/img/6C.png'}, 'value': '6', 'suit': 'CLUBS'},
        {'code': 'QH', 'image': 'https://deckofcardsapi.com/static/img/QH.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/QH.svg', 'png': 'https://deckofcardsapi.com/static/img/QH.png'}, 'value': 'QUEEN', 'suit': 'HEARTS'},
        {'code': '0H', 'image': 'https://deckofcardsapi.com/static/img/0H.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/0H.svg', 'png': 'https://deckofcardsapi.com/static/img/0H.png'}, 'value': '10', 'suit': 'HEARTS'},
        {'code': '9S', 'image': 'https://deckofcardsapi.com/static/img/9S.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/9S.svg', 'png': 'https://deckofcardsapi.com/static/img/9S.png'}, 'value': '9', 'suit': 'SPADES'},
        {'code': '3D', 'image': 'https://deckofcardsapi.com/static/img/3D.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/3D.svg', 'png': 'https://deckofcardsapi.com/static/img/3D.png'}, 'value': '3', 'suit': 'DIAMONDS'},
        {'code': '5H', 'image': 'https://deckofcardsapi.com/static/img/5H.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/5H.svg', 'png': 'https://deckofcardsapi.com/static/img/5H.png'}, 'value': '5', 'suit': 'HEARTS'},
        {'code': '4H', 'image': 'https://deckofcardsapi.com/static/img/4H.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/4H.svg', 'png': 'https://deckofcardsapi.com/static/img/4H.png'}, 'value': '4', 'suit': 'HEARTS'},
        {'code': '5S', 'image': 'https://deckofcardsapi.com/static/img/5S.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/5S.svg', 'png': 'https://deckofcardsapi.com/static/img/5S.png'}, 'value': '5', 'suit': 'SPADES'}]
        
        self.assertEqual({'HEARTS': 4, 'SPADES': 3, 'DIAMONDS': 2, 'CLUBS': 1},compte_des_cartes(cartes1))  

if __name__ =='__main__': 
    unittest.main()