import requests


link = "http://127.0.0.1:8000"



#initialisation du paquet de cartes
def creer_deck():
    req = requests.get(link+"/creer-un-deck/")
    deck = req.json()

    return(deck["deck_id"])
#Tirage des cartes  : 

def draw(id,nombrecartes):

    if id == "": 
        id = creer_deck()
    settings = {"deck_id":id}

    req = requests.post(link+"/cartes/{}".format(nombrecartes),json =settings)

    json = req.json()

    carte = json["cards"]
    
    return(carte)

#Compte des cartes : 

def compte_des_cartes(carte):
    compte = {"HEARTS":0,"SPADES":0,"DIAMONDS":0,"CLUBS":0}
    for el in carte : 
        compte[el["suit"]]+=1
    return compte


#Scénario : 

def scenario(): 
    id = creer_deck()
    carte = draw(id,10)
    compte = compte_des_cartes(carte)
    print("id = ")
    print(carte)
    print(compte)

scenario()