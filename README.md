# TP Conception logicielle

TP noté de Conception de logiciel 2021. 


## Webservice 
Il faut tout d'abord lancer le service ! 
Dans bash depuis la racine: 
```
$ cd Serveur/ 
$ pip install -r requirements.txt
$ uvicorn main:app --reload
```
On peut accéder à cette api via : http://127.0.0.1:8000/docs#

## Client 

Une fois le service lancé, on peut maintenant accéder au côté client, depuis la racine dans Bash : 
``` 
$cd Client/
$pip install -r requirements.txt 
$python main.py ou $python3 main.py
```


## Tests unitaires 

Un seul test est fait sur la fonction de compte des cartes, pour lancer ce test depuis la racine : 
``` 
$python -m unittest tests/Test_Client.py ou $python3 -m unittest tests/Test_Client.py
```