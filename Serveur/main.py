import requests

from fastapi import FastAPI

from metier import deck 


app = FastAPI()

#Fonction utiles pour la création de l'API : 

def creationDeck (): 
    req = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    json = req.json()

    id = json["deck_id"]
    return (id)

def draw (id,nbcartes): 
    if id == "" : 
        id = creationDeck()
        
    req = requests.get("https://deckofcardsapi.com/api/deck/"+id+"/draw/?count={}".format(nbcartes))
    json= req.json()
    res = {"deck id": id ,"cards": json["cards"]}
    
    return(res)

#Fonctions de l'API : 

@app.get("/")
def read_root():
    return {"API : https://deckofcardsapi.com/"}


@app.get("/creer-un-deck/")
def CreerDeck(): 

    id = creationDeck()
    return {"deck_id": id}


@app.post("/cartes/{nombrecartes}")
async def TirageCarte(Item : deck ,nombrecartes : int): 

    res=draw(Item.id,nombrecartes)
    
    return(res)